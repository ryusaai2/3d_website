function navShowModel() {

     let raphaelG = document.getElementById('raphaelG');
     raphaelG.addEventListener('click', () => {
          zoomLibRaphaelG();
          textBoxRaphaelG.style.opacity = 1;
          textBoxRaphaelG.style.zIndex = 1;
     });

     let timS = document.getElementById('timS');
     timS.addEventListener('click', () => {
          zoomLibTimS();
          textBoxTimS.style.opacity = 1;
          textBoxTimS.style.zIndex = 1;
     });

     let chinL = document.getElementById('chinL');
     chinL.addEventListener('click', () => {
          zoomLibChinL();
          textBoxChinL.style.opacity = 1;
          textBoxChinL.style.zIndex = 1;
     });

     let sandroS = document.getElementById('sandroS');
     sandroS.addEventListener('click', () => {
          zoomLibSandroS();
          textBoxSandroS.style.opacity = 1;
          textBoxSandroS.style.zIndex = 1;
     });

     let rezaS = document.getElementById('rezaS');
     rezaS.addEventListener('click', () => {
          zoomLibRezaS();
          textBoxRezaS.style.opacity = 1;
          textBoxRezaS.style.zIndex = 1;
     });

     let philippT = document.getElementById('philippT');
     philippT.addEventListener('click', () => {
          zoomLibPhilippT();
          textBoxPhilippT.style.opacity = 1;
          textBoxPhilippT.style.zIndex = 1;
     });

     let kevinR = document.getElementById('kevinR');
     kevinR.addEventListener('click', () => {
          zoomLibKevinR();
          modelAnimation = true;
          textBoxKevinR.style.opacity = 1;
          textBoxKevinR.style.zIndex = 1;
     });

     let jessicaE = document.getElementById('jessicaE');
     jessicaE.addEventListener('click', () => {
          zoomLibJessicaE();
          textBoxJessicaE.style.opacity = 1;
          textBoxJessicaE.style.zIndex = 1;
     });

     let nicolasI = document.getElementById('nicolasI');
     nicolasI.addEventListener('click', () => {
          zoomLibNicolasI();
          textBoxNicolasI.style.opacity = 1;
          textBoxNicolasI.style.zIndex = 1;
     });
}
