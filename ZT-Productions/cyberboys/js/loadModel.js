function cyberboysEnvironment() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/cyberboys_environment/Environment_1.gltf', function( gltf ) {
          envModel = gltf.scene;
          envModel.scale.set(0.25, 0.25, 0.25);
          envModel.rotation.y = Math.PI / -2;
          envModel.position.set(0, -35, -150);
          scene.add( envModel );
          //Add Shadow
          gltf.scene.traverse( function ( child ) {

              if ( child.isMesh ) {

                  child.castShadow = true;
                  child.receiveShadow = true;

                }
              })
         }, manager.onProgress, manager.onError );
}//Ende cyberboysEnvironment

//Dealer
function raphaelG() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/raphi/raphi.gltf', function( gltf ) {
          raphael = gltf.scene;
          raphael.position.set(-75, -36, -20);
          raphael.scale.set(25, 25, 25);
          raphael.rotation.set(0, -300, 0);
          scene.add( raphael );
          const animation = gltf.animations[ 0 ];
          const mixer = new THREE.AnimationMixer( raphael );
          mixers.push( mixer );
          const animationRaphael = gltf.animations[ 0 ];

          raphaelAnimation.addEventListener( 'click', play, false);

          function play( event ) {
               const action = mixer.clipAction( animationRaphael );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }
          zoomPointRaphaelG();
          //Add Shadow
          gltf.scene.traverse( function ( child ) {

              if ( child.isMesh ) {

                  child.castShadow = true;
                  child.receiveShadow = false;

                }
              })
         }, manager.onProgress, manager.onError );
}//Ende raphaelG

function zoomPointRaphaelG() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/symbol/indicator.gltf', function( gltf ) {
          zoomPointRaphaelG = gltf.scene;
          zoomPointRaphaelG.scale.set(0.2, 0.2, 0.2);
          zoomPointRaphaelG.position.set(-75, 17.5, -20);
          scene.add( zoomPointRaphaelG );
          const animation = gltf.animations[ 0 ];
          const mixer = new THREE.AnimationMixer( zoomPointRaphaelG );
          mixers.push( mixer );
          const action = mixer.clipAction( animation );
          action.play();
          let domEvents = new THREEx.DomEvents(camera, renderer.domElement);
          domEvents.addEventListener(zoomPointRaphaelG, "click", event => {
               zoomLibRaphaelG();
               textBoxRaphaelG.style.opacity = 1;
               textBoxRaphaelG.style.zIndex = 1;
          }); //Ende DomEvent
     }, manager.onProgress, manager.onError );
}//Ende zoomPointRaphaelG

// Ecke unten
function timS() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/tim/tim.glb', function( gltf ) {
          tim = gltf.scene;
          tim.position.set(70, -36, 10);
          tim.scale.set(25, 25, 25);
          tim.rotation.set(0, 300, 0);
          scene.add( tim );
          const animation = gltf.animations[ 0 ];
          const mixer = new THREE.AnimationMixer( tim );
          mixers.push( mixer );

          const animationTim = gltf.animations[ 0 ];

          timAnimation.addEventListener( 'click', play, false);

          function play( event ) {
               const action = mixer.clipAction( animationTim );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }

          zoomPointTimS();
          //Add Shadow
          gltf.scene.traverse( function ( child ) {

              if ( child.isMesh ) {

                  child.castShadow = true;
                  child.receiveShadow = false;

                }
              })
         }, manager.onProgress, manager.onError );
}//Ende timS


function zoomPointTimS() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/symbol/indicator.gltf', function( gltf ) {
          zoomPointTimS = gltf.scene;
          zoomPointTimS.scale.set(0.2, 0.2, 0.2);
          zoomPointTimS.position.set(70, 17.5, 10);
          scene.add( zoomPointTimS );
          const animation = gltf.animations[ 0 ];
          const mixer = new THREE.AnimationMixer( zoomPointTimS );
          mixers.push( mixer );
          const action = mixer.clipAction( animation );
          action.play();
          let domEvents = new THREEx.DomEvents(camera, renderer.domElement);
          domEvents.addEventListener(zoomPointTimS, "click", event => {
               zoomLibTimS();
               textBoxTimS.style.opacity = 1;
               textBoxTimS.style.zIndex = 1;
          }); //Ende DomEvent
     }, manager.onProgress, manager.onError );
}//Ende zoomPointTimS

//Barkeeper
function chinL() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/chin/chin.gltf', function( gltf ) {
          chin = gltf.scene;
          chin.position.set(70, -36, -125);
          chin.scale.set(25, 25, 25);
          chin.rotation.set(0, 100, 0);
          scene.add( chin );
          const animation = gltf.animations[ 0 ];
          const mixer = new THREE.AnimationMixer( chin );
          mixers.push( mixer );
          const animationChin = gltf.animations[ 0 ];

          chinAnimation.addEventListener( 'click', play, false);

          function play( event ) {
               const action = mixer.clipAction( animationChin );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }

          zoomPointChinL();
          //Add Shadow
          gltf.scene.traverse( function ( child ) {

              if ( child.isMesh ) {

                  child.castShadow = true;
                  child.receiveShadow = false;

                }
              })
         }, manager.onProgress, manager.onError );
}//Ende chinL

function zoomPointChinL() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/symbol/indicator.gltf', function( gltf ) {
          zoomPointChinL = gltf.scene;
          zoomPointChinL.scale.set(0.2, 0.2, 0.2);
          zoomPointChinL.position.set(70, 19.5, -125);
          scene.add( zoomPointChinL );
          const animation = gltf.animations[ 0 ];
          const mixer = new THREE.AnimationMixer( zoomPointChinL );
          mixers.push( mixer );
          const action = mixer.clipAction( animation );
          action.play();
          let domEvents = new THREEx.DomEvents(camera, renderer.domElement);
          domEvents.addEventListener(zoomPointChinL, "click", event => {
               zoomLibChinL();
               textBoxChinL.style.opacity = 1;
               textBoxChinL.style.zIndex = 1;
          }); //Ende DomEvent
     }, manager.onProgress, manager.onError );
}//Ende zoomPointChinL

// Tisch1
function sandroS() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/Sandro.glb', function( gltf ) {
          sandro = gltf.scene;
          sandro.position.set(-25, -53, -170);
          sandro.scale.set(25, 25, 25);
          sandro.rotation.set(0, 0.4, 0);
          sandro.castShadow = true;
          sandro.receiveShadow = true;
          scene.add( sandro );
          const animation = gltf.animations[ 0 ];
          const mixer = new THREE.AnimationMixer( sandro );
          mixers.push( mixer );
          const animationSandro = gltf.animations[ 0 ];

          sandroAnimation.addEventListener( 'click', play, false);

          function play( event ) {
               const action = mixer.clipAction( animationSandro );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }
          zoomPointSandroS();
          //Add Shadow
          gltf.scene.traverse( function ( child ) {

              if ( child.isMesh ) {

                  child.castShadow = true;
                  child.receiveShadow = false;

                }
              })
         }, manager.onProgress, manager.onError );
}//Ende sandroS

function zoomPointSandroS() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/symbol/indicator.gltf', function( gltf ) {
          zoomPointSandroS = gltf.scene;
          zoomPointSandroS.scale.set(0.2, 0.2, 0.2);
          zoomPointSandroS.position.set(-22, 12, -175);
          scene.add( zoomPointSandroS );
          const animation = gltf.animations[ 0 ];
          const mixer = new THREE.AnimationMixer( zoomPointSandroS );
          mixers.push( mixer );
          const action = mixer.clipAction( animation );
          action.play();
          let domEvents = new THREEx.DomEvents(camera, renderer.domElement);
          domEvents.addEventListener(zoomPointSandroS, "click", event => {
               zoomLibSandroS();
               textBoxSandroS.style.opacity = 1;
               textBoxSandroS.style.zIndex = 1;
          }); //Ende DomEvent
     }, manager.onProgress, manager.onError );
}//Ende zoomPointSandroS

// Tisch2
function rezaS() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/reza.glb', function( gltf ) {
          reza = gltf.scene;
          reza.position.set(-67.5, -47, -120);
          reza.scale.set(25, 25, 25);
          reza.rotation.set(0, -250, 0);
          scene.add( reza );
          const animation = gltf.animations[ 0 ];
          const mixer = new THREE.AnimationMixer( reza );
          mixers.push( mixer );
          const animationReza = gltf.animations[ 0 ];

          rezaAnimation.addEventListener( 'click', play, false);

          function play( event ) {
               const action = mixer.clipAction( animationReza );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }
          zoomPointRezaS();
          //Add Shadow
          gltf.scene.traverse( function ( child ) {

              if ( child.isMesh ) {

                  child.castShadow = true;
                  child.receiveShadow = false;

                }
              })
         }, manager.onProgress, manager.onError );
}//Ende rezaS

function zoomPointRezaS() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/symbol/indicator.gltf', function( gltf ) {
          zoomPointRezaS = gltf.scene;
          zoomPointRezaS.scale.set(0.2, 0.2, 0.2);
          zoomPointRezaS.position.set(-80, 12, -120);
          scene.add( zoomPointRezaS );
          const animation = gltf.animations[ 0 ];
          const mixer = new THREE.AnimationMixer( zoomPointRezaS );
          mixers.push( mixer );
          const action = mixer.clipAction( animation );
          action.play();
          let domEvents = new THREEx.DomEvents(camera, renderer.domElement);
          domEvents.addEventListener(zoomPointRezaS, "click", event => {
               zoomLibRezaS();
               textBoxRezaS.style.opacity = 1;
               textBoxRezaS.style.zIndex = 1;
          }); //Ende DomEvent
     }, manager.onProgress, manager.onError );
}//Ende zoomPointRezaS

// Tisch3
function philippT() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/philipp/philipp_anim.gltf', function( gltf ) {
          philipp = gltf.scene;
          philipp.position.set(-55, -37.5, -245);
          philipp.scale.set(25, 25, 25);
          philipp.rotation.set(0, -50, 0);
          scene.add( philipp );
          const animation = gltf.animations[ 0 ];
          const mixer = new THREE.AnimationMixer( philipp );
          mixers.push( mixer );
          const animationPhilipp = gltf.animations[ 0 ];

          philippAnimation.addEventListener( 'click', play, false);

          function play( event ) {
               const action = mixer.clipAction( animationPhilipp );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }
          zoomPointPhilippT();
          //Add Shadow
          gltf.scene.traverse( function ( child ) {

              if ( child.isMesh ) {

                  child.castShadow = true;
                  child.receiveShadow = false;

                }
              })
         }, manager.onProgress, manager.onError );
}//Ende philippT

function zoomPointPhilippT() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/symbol/indicator.gltf', function( gltf ) {
          zoomPointPhilippT = gltf.scene;
          zoomPointPhilippT.scale.set(0.2, 0.2, 0.2);
          zoomPointPhilippT.position.set(-55, 8, -250);
          scene.add( zoomPointPhilippT );
          const animation = gltf.animations[ 0 ];
          const mixer = new THREE.AnimationMixer( zoomPointPhilippT );
          mixers.push( mixer );
          const action = mixer.clipAction( animation );
          action.play();
          let domEvents = new THREEx.DomEvents(camera, renderer.domElement);
          domEvents.addEventListener(zoomPointPhilippT, "click", event => {
               zoomLibPhilippT();
               textBoxPhilippT.style.opacity = 1;
               textBoxPhilippT.style.zIndex = 1;
          }); //Ende DomEvent
     }, manager.onProgress, manager.onError );
}//Ende zoomPointPhilippT

// Neben Bar
function kevinR() {

     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/kevin/kevin.gltf', function( gltf ) {
          kevin = gltf.scene;
          kevin.position.set(25, -36, -220);
          kevin.scale.set(25, 25, 25);
          kevin.rotation.set(0, -250, 0);
          scene.add( kevin );
          const animation = gltf.animations[ 0 ];
          const mixer = new THREE.AnimationMixer( kevin );
          mixers.push( mixer );
          const animationKevin = gltf.animations[ 0 ];

          kevinAnimation.addEventListener( 'click', play, false);

          function play( event ) {
               const action = mixer.clipAction( animationKevin );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }

          //Add Shadow
          gltf.scene.traverse( function ( child ) {

              if ( child.isMesh ) {

                  child.castShadow = true;
                  child.receiveShadow = false;

               }
           })
              zoomPointKevinR();

         }, manager.onProgress, manager.onError );
}//Ende kevinR

function zoomPointKevinR() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/symbol/indicator.gltf', function( gltf ) {
          zoomPointKevinR = gltf.scene;
          zoomPointKevinR.scale.set(0.2, 0.2, 0.2);
          zoomPointKevinR.position.set(25, 17.5, -220);
          scene.add( zoomPointKevinR );
          const animation = gltf.animations[ 0 ];
          const mixer = new THREE.AnimationMixer( zoomPointKevinR );
          mixers.push( mixer );
          const action = mixer.clipAction( animation );
          action.play();
          let domEvents = new THREEx.DomEvents(camera, renderer.domElement);
          domEvents.addEventListener(zoomPointKevinR, "click", event => {
               zoomLibKevinR();
               textBoxKevinR.style.opacity = 1;
               textBoxKevinR.style.zIndex = 1;
          }); //Ende DomEvent
     }, manager.onProgress, manager.onError );
}//Ende zoomPointKevinR

// Ecke oben
function jessicaE() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/jessica/Jessica.gltf', function( gltf ) {
          jessica = gltf.scene;
          jessica.position.set(-50, 30, -295);
          jessica.scale.set(25, 25, 25);
          jessica.rotation.set(0, -100, 0);
          scene.add( jessica );
          const animation = gltf.animations[ 0 ];
          const mixer = new THREE.AnimationMixer( jessica );
          mixers.push( mixer );
          const animationJessica = gltf.animations[ 0 ];

          jessicaAnimation.addEventListener( 'click', play, false);

          function play( event ) {
               const action = mixer.clipAction( animationJessica );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }
          zoomPointJessicaE();
          //Add Shadow
          gltf.scene.traverse( function ( child ) {

              if ( child.isMesh ) {

                  child.castShadow = true;
                  child.receiveShadow = false;

                }
              })
         }, manager.onProgress, manager.onError );
}//Ende jessicaE

function zoomPointJessicaE() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/symbol/indicator.gltf', function( gltf ) {
          zoomPointJessicaE = gltf.scene;
          zoomPointJessicaE.scale.set(0.2, 0.2, 0.2);
          zoomPointJessicaE.position.set(-47.5, 80, -295);
          scene.add( zoomPointJessicaE );
          const animation = gltf.animations[ 0 ];
          const mixer = new THREE.AnimationMixer( zoomPointJessicaE );
          mixers.push( mixer );
          const action = mixer.clipAction( animation );
          action.play();
          let domEvents = new THREEx.DomEvents(camera, renderer.domElement);
          domEvents.addEventListener(zoomPointJessicaE, "click", event => {
               zoomLibJessicaE();
               textBoxJessicaE.style.opacity = 1;
               textBoxJessicaE.style.zIndex = 1;
          }); //Ende DomEvent
     }, manager.onProgress, manager.onError );
}//Ende zoomPointJessicaE

//Dancer
function nicolasI() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/nici.glb', function( gltf ) {
          nicolas = gltf.scene;
          nicolas.position.set(55, 30, -100);
          nicolas.scale.set(25, 25, 25);
          nicolas.rotation.set(0, 150, 0);
          scene.add( nicolas );
          const animation = gltf.animations[ 0 ];
          const mixer = new THREE.AnimationMixer( nicolas );
          mixers.push( mixer );
          const animationNicolas = gltf.animations[ 0 ];

          nicolasAnimation.addEventListener( 'click', play, false);

          function play( event ) {
               const action = mixer.clipAction( animationNicolas );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }
          zoomPointNicolasI();
          //Add Shadow
          gltf.scene.traverse( function ( child ) {

              if ( child.isMesh ) {

                  child.castShadow = true;
                  child.receiveShadow = false;

                }
              })
         }, manager.onProgress, manager.onError );
}//Ende nicolasI

function zoomPointNicolasI() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/symbol/indicator.gltf', function( gltf ) {
          zoomPointNicolasI = gltf.scene;
          zoomPointNicolasI.scale.set(0.2, 0.2, 0.2);
          zoomPointNicolasI.position.set(55, 80, -100);
          scene.add( zoomPointNicolasI );
          const animation = gltf.animations[ 0 ];
          const mixer = new THREE.AnimationMixer( zoomPointNicolasI );
          mixers.push( mixer );
          const action = mixer.clipAction( animation );
          action.play();
          let domEvents = new THREEx.DomEvents(camera, renderer.domElement);
          domEvents.addEventListener(zoomPointNicolasI, "click", event => {
               zoomLibNicolasI();
               textBoxNicolasI.style.opacity = 1;
               textBoxNicolasI.style.zIndex = 1;
          }); //Ende DomEvent
     }, manager.onProgress, manager.onError );
}//Ende zoomPointNicolasI
