function displayNone() {
     arrowLeft.style.display = 'none';
     arrowRight.style.display = 'none';

     textBoxRaphaelG.style.opacity = 0;
     textBoxRaphaelG.style.zIndex = 0;
     textBoxTimS.style.opacity = 0;
     textBoxTimS.style.zIndex = 0;
     textBoxChinL.style.opacity = 0;
     textBoxChinL.style.zIndex = 0;
     textBoxSandroS.style.opacity = 0;
     textBoxSandroS.style.zIndex = 0;
     textBoxRezaS.style.opacity = 0;
     textBoxRezaS.style.zIndex = 0;
     textBoxPhilippT.style.opacity = 0;
     textBoxPhilippT.style.zIndex = 0;
     textBoxKevinR.style.opacity = 0;
     textBoxKevinR.style.zIndex = 0;
     textBoxJessicaE.style.opacity = 0;
     textBoxJessicaE.style.zIndex = 0;
     textBoxNicolasI.style.opacity = 0;
     textBoxNicolasI.style.zIndex = 0;

     button.style.display = 'none';

     scene.remove( zoomPointRaphaelG );
     scene.remove( zoomPointTimS );
     scene.remove( zoomPointChinL );
     scene.remove( zoomPointRezaS );
     scene.remove( zoomPointSandroS );
     scene.remove( zoomPointPhilippT );
     scene.remove( zoomPointKevinR );
     scene.remove( zoomPointJessicaE );
     scene.remove( zoomPointNicolasI );
}

function displayON() {
     scene.add( zoomPointRaphaelG );
     scene.add( zoomPointTimS );
     scene.add( zoomPointChinL );
     scene.add( zoomPointRezaS );
     scene.add( zoomPointSandroS );
     scene.add( zoomPointPhilippT);
     scene.add( zoomPointKevinR  );
     scene.add( zoomPointJessicaE );
     scene.add( zoomPointNicolasI );
}
