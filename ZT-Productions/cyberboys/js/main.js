//FPS anzeige
//(function(){var script=document.createElement('script');script.onload=function(){var stats=new Stats();document.body.appendChild(stats.dom);requestAnimationFrame(function loop(){stats.update();requestAnimationFrame(loop)});};script.src='//mrdoob.github.io/stats.js/build/stats.min.js';document.head.appendChild(script);})()

//Alle Variablen
let container;
let camera;
let controls;
let renderer;
let scene;
let modelLoad;

const clock = new THREE.Clock();
const manager = initLoadingManager(); //LadeScreen
const mixers = []; //Animation

function init() {
     container = document.querySelector( '#scene-container' );
     scene = new THREE.Scene();

     createCamera();
     createControls();
     createLights();
     createRenderer();
     loadModels();

     renderer.setAnimationLoop( () => {
          update();
          render();
     } );
}

//Kamera
function createCamera() {
     camera = new THREE.PerspectiveCamera( 45, container.clientWidth / container.clientHeight, 1, 5000 );
     camera.position.set(0, 0, 0.5);
     camera.rotation.set(0, 0, 0);
}

//Kontrolle
function createControls() {
     controls = new THREE.OrbitControls( camera, container );
     controls.enableZoom = false;
     controls.enablePan = false;
     controls.target.set(0, 0, 0);
     controls.update();
}

//Licht
function createLights() {
     const light = new THREE.PointLight(0xFFFFFF, 2);
     light.castShadow = true;
     light.position.set(-50, 50, -150);
     light.shadow.mapSize.width = 2048;
     light.shadow.mapSize.height = 2048;

     scene.add(light);

     lightHelper = new THREE.PointLightHelper( light, 25 );
     lightHelper.position.set( -50, 50, -150 );
     //scene.add( lightHelper );

     var light2 = new THREE.AmbientLight( 0x404040, 1.5 );
     scene.add( light2 );

     scene.fog = new THREE.FogExp2( 0xd9e8ff, 0.00125);
}

//Model laden
function loadModels() {
     cyberboysEnvironment();
     navShowModel();
     raphaelG();
     timS();
     chinL();
     sandroS();
     rezaS();
     philippT();
     kevinR();
     jessicaE();
     nicolasI();
     newPos();
}

//Rendern
function createRenderer() {
     renderer = new THREE.WebGLRenderer( { antialias: true } );
     renderer.setSize( container.clientWidth, container.clientHeight );
     renderer.setPixelRatio( window.devicePixelRatio );
     renderer.shadowMap.enabled = true;
     renderer.shadowMap.type = THREE.PCSoftShadowMap;
     container.appendChild( renderer.domElement );
}

function update() {
     //Animation abspielen
     const delta = clock.getDelta();
     for ( const mixer of mixers ) {
          mixer.update( delta );
     }

     //Zoomen
     TWEEN.update();
     requestAnimationFrame( update );
}

function render() {
     renderer.render( scene, camera );
}

//Responsive
function onWindowResize() {
     camera.aspect = container.clientWidth / container.clientHeight;
     camera.updateProjectionMatrix();
     renderer.setSize( container.clientWidth, container.clientHeight );
}

window.addEventListener( 'resize', onWindowResize )

init();


///////////////////////////////////////////////////////////////////////////////////

//LadeScreen
function initLoadingManager() {
     const manager = new THREE.LoadingManager();
     const progressBar = document.querySelector( '#progress' );
     const loadingOverlay = document.querySelector( '#loading-overlay' );
     let percentComplete = 1;
     let frameID = null;
     const updateAmount = 12.35; // in percent of bar width, should divide 100 evenly
     const animateBar = () => {
          percentComplete += updateAmount;
          progressBar.style.width = percentComplete + '%';
          frameID = requestAnimationFrame( animateBar )
     }
     manager.onStart = () => { // prevent the timer being set again, if onStart is called multiple times
          if ( frameID !== null ) return;
              animateBar();
     };
     manager.onLoad = () => { // reset the bar in case we need to use it again
          loadingOverlay.classList.add( 'loading-overlay-hidden' );
          percentComplete = 0;
          progressBar.style.width = 0;
          cancelAnimationFrame( frameID );
     };
     manager.onError = function ( e ) {
          console.error( e );
          progressBar.style.backgroundColor = 'red';
     }
          return manager;
}

///////////////////////////////////////////////////////////////////////////////////
//Textboxen
let textBoxRaphaelG = document.getElementById('textbox-raphaelG');
let textBoxTimS = document.getElementById('textbox-timS');
let textBoxChinL = document.getElementById('textbox-chinL');
let textBoxSandroS = document.getElementById('textbox-sandroS');
let textBoxRezaS = document.getElementById('textbox-rezaS');
let textBoxPhilippT = document.getElementById('textbox-philippT');
let textBoxKevinR = document.getElementById('textbox-kevinR');
let textBoxJessicaE = document.getElementById('textbox-jessicaE');
let textBoxNicolasI = document.getElementById('textbox-nicolasI');

let button = document.getElementById('zoom-out');
let arrowLeft = document.getElementById('arrow-left');
let arrowRight = document.getElementById('arrow-right');
let showNewPosition = document.getElementById('show-new-position');
let sceneContainer = document.getElementById('scene-container');
let customPosition = document.getElementById('custom-position');
let customPositionActive = document.getElementById('custom-position-active');
let customView = document.getElementById('custom-view');
let customViewActive = document.getElementById('custom-view-active');

//Animationen Characters
let timAnimation = document.getElementById('tim-animation');
let raphaelAnimation = document.getElementById('raphael-animation');
let chinAnimation = document.getElementById('chin-animation');
let sandroAnimation = document.getElementById('sandro-animation');
let rezaAnimation = document.getElementById('reza-animation');
let philippAnimation = document.getElementById('philipp-animation');
let kevinAnimation = document.getElementById('kevin-animation');
let jessicaAnimation = document.getElementById('jessica-animation');
let nicolasAnimation = document.getElementById('nicolas-animation');


jQuery('.faq__accordian-heading').click(function(e){
        e.preventDefault();
        if (!jQuery(this).hasClass('active')) {
            jQuery('.faq__accordian-heading').removeClass('active');
            jQuery('.faq__accordion-content').slideUp(500);
            jQuery(this).addClass('active');
            jQuery(this).next('.faq__accordion-content').slideDown(500);
        }
        else if (jQuery(this).hasClass('active')) {
            jQuery(this).removeClass('active');
            jQuery(this).next('.faq__accordion-content').slideUp(500);
        }
});

jQuery('.nav-custom_accordion-heading').click(function(e){
     e.preventDefault();
          if (!jQuery(this).hasClass('active')) {
               jQuery('.nav-custom_accordion-heading').removeClass('active');
               jQuery('.nav-custom_accordion-content').slideUp(500);
               jQuery(this).addClass('active');
               jQuery(this).next('.nav-custom_accordion-content').slideDown(500);
          }
          else if (jQuery(this).hasClass('active')) {
               jQuery(this).removeClass('active');
               jQuery(this).next('.nav-custom_accordion-content').slideUp(500);
          }
});
