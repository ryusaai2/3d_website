//zoomLibRaphaelG
function zoomLibRaphaelG() {
     camera.rotation.set(0, 1, 0);
     camera.position.set(0, 0, 0.5);
     displayNone();
     zoomIn(-35, 0, -15);
     addArrowRaphaelG();
     //button um zurückzukehren
     let zoomOutRaphaelG = document.getElementById('zoom-out');
     zoomOutRaphaelG.style.display = 'block';
     //button click event
     zoomOutRaphaelG.addEventListener('click', () => {
          zoomOut(35, 0, 25);
          displayNone();
          displayON();
          controls.target.set(-15, 0, 0);
     })//Ende Close Button
} // Ende zoomLibRaphaelG

//zoomLibTimS
function zoomLibTimS() {
     camera.rotation.set(0, -2, 0);
     camera.position.set(0, 0, 0.5);
     displayNone();
     zoomIn(35, 0, 5);
     addArrowTimS();
     //button um zurückzukehren
     let zoomOutTimS = document.getElementById('zoom-out');
     zoomOutTimS.style.display = 'block';
     //button click event
     zoomOutTimS.addEventListener('click', () => {
          zoomOut(-35, 0, -5);
          displayNone();
          displayON();
          controls.target.set(1, 0, 1);
     })//Ende Close Button
} // Ende zoomLibTimS

//zoomLibChinL
function zoomLibChinL() {
     camera.rotation.set(0, -0.75, 0);
     camera.position.set(0, 0, 0.5);
     displayNone();
     zoomIn(50, 5, -90);
     addArrowChinL();
     //button um zurückzukehren
     let zoomOutChinL = document.getElementById('zoom-out');
     zoomOutChinL.style.display = 'block';
     //button click event
     zoomOutChinL.addEventListener('click', () => {
          zoomOut(-50, -5, 90);
          displayNone();
          displayON();
          controls.target.set(0.5, 0, 0);
     })//Ende Close Button
} // Ende zoomLibChinL

//zoomLibSandroS
function zoomLibSandroS() {
     camera.rotation.set(0, 0, 0);
     camera.position.set(0, 0, 0.5);
     displayNone();
     zoomIn(-10, -7.5, -150);
     addArrowSandroS();
     //button um zurückzukehren
     let zoomOutSandroS = document.getElementById('zoom-out');
     zoomOutSandroS.style.display = 'block';
     //button click event
     zoomOutSandroS.addEventListener('click', () => {
          zoomOut(10, 7.5, 150);
          displayNone();
          displayON();
          controls.target.set(0, 0, 0);
     })//Ende Close Button
} // Ende zoomLibSandroS

//zoomLibRezaS
function zoomLibRezaS() {
     camera.rotation.set(0, 0.5, 0);
     camera.position.set(0, 0, 0.5);
     displayNone();
     zoomIn(-55, -7.5, -95);
     addArrowRezaS();
     //button um zurückzukehren
     let zoomOutRezaS = document.getElementById('zoom-out');
     zoomOutRezaS.style.display = 'block';
     //button click event
     zoomOutRezaS.addEventListener('click', () => {
          zoomOut(55, 7.5, 95);
          displayNone();
          displayON();
          controls.target.set(-0.3, 0, 0);
     })//Ende Close Button
} // Ende zoomLibRezaS

//zoomLibPhilippT
function zoomLibPhilippT() {
     camera.rotation.set(0, 0, 0);
     camera.position.set(0, 0, 0.5);
     displayNone();
     zoomIn(-40, -11, -215);
     addArrowPhilippT();
     //button um zurückzukehren
     let zoomOutPhilippT = document.getElementById('zoom-out');
     zoomOutPhilippT.style.display = 'block';
     //button click event
     zoomOutPhilippT.addEventListener('click', () => {
          zoomOut(40, 11, 215);
          displayNone();
          displayON();
          controls.target.set(0, 0, 0);
     })//Ende Close Button
} // Ende zoomLibPhilippT

//zoomLibKevinR
function zoomLibKevinR() {
     camera.rotation.set(0, 0, 0);
     camera.position.set(0, 0, 0.5);
     displayNone();
     zoomIn(40, 0, -185);
     addArrowKevinR();
     //button um zurückzukehren
     let zoomOutKevinR = document.getElementById('zoom-out');
     zoomOutKevinR.style.display = 'block';
     //button click event
     zoomOutKevinR.addEventListener('click', () => {
          zoomOut(-40, 0, 185);
          displayNone();
          displayON();
          controls.target.set(0, 0, 0);
     })//Ende Close Button
} // Ende zoomLibKevinR

//zoomLibJessicaE
function zoomLibJessicaE() {
     camera.rotation.set(0, 0, 0);
     camera.position.set(0, 0, 0.5);
     displayNone();
     zoomIn(-37.5, 60, -265);
     addArrowJessicaE();
     //button um zurückzukehren
     let zoomOutJessicaE = document.getElementById('zoom-out');
     zoomOutJessicaE.style.display = 'block';
     //button click event
     zoomOutJessicaE.addEventListener('click', () => {
          zoomOut(35, -60, 250);
          displayNone();
          displayON();
          controls.target.set(0, 0, 0);
     })//Ende Close Button
} // Ende zoomLibJessicaE

//zoomLibNicolasI
function zoomLibNicolasI() {
     camera.rotation.set(0, -0.75, 0);
     camera.position.set(0, 0, 0.5);
     displayNone();
     zoomIn(40, 65, -65);
     addArrowNicolasI();
     //button um zurückzukehren
     let zoomOutNicolasI = document.getElementById('zoom-out');
     zoomOutNicolasI.style.display = 'block';
     //button click event
     zoomOutNicolasI.addEventListener('click', () => {
          zoomOut(-40, -65, 65);
          displayNone();
          displayON();
          controls.target.set(0.5, 0, 0);
     })//Ende Close Button
} // Ende zoomLibNicolasI
