
function addArrowRaphaelG() {
     arrowLeft.style.display = 'block';
     arrowRight.style.display = 'block';
     arrowLeft.addEventListener('click', () => {
          zoomLibNicolasI();
          textBoxNicolasI.style.opacity = 1;
          textBoxNicolasI.style.zIndex = 1;
     });//Ende arrowLeft.addEventListener
     arrowRight.addEventListener('click', () => {
          zoomLibTimS();
          textBoxTimS.style.opacity = 1;
          textBoxTimS.style.zIndex = 1;
     });//Ende arrowRight.addEventListener
}

function addArrowTimS() {
     arrowLeft.style.display = 'block';
     arrowRight.style.display = 'block';
     arrowLeft.addEventListener('click', () => {
          zoomLibRaphaelG();
          textBoxRaphaelG.style.opacity = 1;
          textBoxRaphaelG.style.zIndex = 1;
     });//Ende arrowLeft.addEventListener
     arrowRight.addEventListener('click', () => {
          zoomLibChinL();
          textBoxChinL.style.opacity = 1;
          textBoxChinL.style.zIndex = 1;
     });//Ende arrowRight.addEventListener
}

function addArrowChinL() {
     arrowLeft.style.display = 'block';
     arrowRight.style.display = 'block';
     arrowLeft.addEventListener('click', () => {
          zoomLibTimS();
          textBoxTimS.style.opacity = 1;
          textBoxTimS.style.zIndex = 1;
     });//Ende arrowLeft.addEventListener
     arrowRight.addEventListener('click', () => {
          zoomLibSandroS();
          textBoxSandroS.style.opacity = 1;
          textBoxSandroS.style.zIndex = 1;
     });//Ende arrowRight.addEventListener
}

function addArrowSandroS() {
     arrowLeft.style.display = 'block';
     arrowRight.style.display = 'block';
     arrowLeft.addEventListener('click', () => {
          zoomLibChinL();
          textBoxChinL.style.opacity = 1;
          textBoxChinL.style.zIndex = 1;
     });//Ende arrowLeft.addEventListener
     arrowRight.addEventListener('click', () => {
          zoomLibRezaS();
          textBoxRezaS.style.opacity = 1;
          textBoxRezaS.style.zIndex = 1;
     });//Ende arrowRight.addEventListener
}

function addArrowRezaS() {
     arrowLeft.style.display = 'block';
     arrowRight.style.display = 'block';
     arrowLeft.addEventListener('click', () => {
          zoomLibSandroS();
          textBoxSandroS.style.opacity = 1;
          textBoxSandroS.style.zIndex = 1;
     });//Ende arrowLeft.addEventListener
     arrowRight.addEventListener('click', () => {
          zoomLibPhilippT();
          textBoxPhilippT.style.opacity = 1;
          textBoxPhilippT.style.zIndex = 1;
     });//Ende arrowRight.addEventListener
}

function addArrowPhilippT() {
     arrowLeft.style.display = 'block';
     arrowRight.style.display = 'block';
     arrowLeft.addEventListener('click', () => {
          zoomLibRezaS();
          textBoxRezaS.style.opacity = 1;
          textBoxRezaS.style.zIndex = 1;
     });//Ende arrowLeft.addEventListener
     arrowRight.addEventListener('click', () => {
          zoomLibKevinR();
          textBoxKevinR.style.opacity = 1;
          textBoxKevinR.style.zIndex = 1;
     });//Ende arrowRight.addEventListener
}

function addArrowKevinR() {
     arrowLeft.style.display = 'block';
     arrowRight.style.display = 'block';
     arrowLeft.addEventListener('click', () => {
          zoomLibPhilippT();
          textBoxPhilippT.style.opacity = 1;
          textBoxPhilippT.style.zIndex = 1;
     });//Ende arrowLeft.addEventListener
     arrowRight.addEventListener('click', () => {
          zoomLibJessicaE();
          textBoxJessicaE.style.opacity = 1;
          textBoxJessicaE.style.zIndex = 1;
     });//Ende arrowRight.addEventListener
}

function addArrowJessicaE() {
     arrowLeft.style.display = 'block';
     arrowRight.style.display = 'block';
     arrowLeft.addEventListener('click', () => {
          zoomLibKevinR();
          textBoxKevinR.style.opacity = 1;
          textBoxKevinR.style.zIndex = 1;
     });//Ende arrowLeft.addEventListener
     arrowRight.addEventListener('click', () => {
          zoomLibNicolasI();
          textBoxNicolasI.style.opacity = 1;
          textBoxNicolasI.style.zIndex = 1;
     });//Ende arrowRight.addEventListener
}

function addArrowNicolasI() {
     arrowLeft.style.display = 'block';
     arrowRight.style.display = 'block';
     arrowLeft.addEventListener('click', () => {
          zoomLibJessicaE();
          textBoxJessicaE.style.opacity = 1;
          textBoxJessicaE.style.zIndex = 1;
     });//Ende arrowLeft.addEventListener
     arrowRight.addEventListener('click', () => {
          zoomLibRaphaelG();
          textBoxRaphaelG.style.opacity = 1;
          textBoxRaphaelG.style.zIndex = 1;
     });//Ende arrowRight.addEventListener
}
