function newPos() {
     //Start
     let pos0 = document.getElementById('pos0');
     pos0.addEventListener('click', () => {
          fadeInOut();
          displayNone();
          displayON();
          setTimeout(() => {
               camera.position.set(0, 0, 0.5);
               camera.rotation.set(0, 0, 0);
               controls.target.set(0, 0, 0);
               controls.enabled = true;
          }, 1000)
     });

     //Pos1
     let pos1 = document.getElementById('pos1');
     pos1.addEventListener('click', () => {
          fadeInOut();
          displayNone();
          displayON();
          setTimeout(() => {
               camera.position.set(0, 0, -250.5);
               camera.rotation.set(0, 9.4, 0);
               controls.target.set(0, 0, -250);
               controls.enabled = true;
          }, 1000)
     });

     //Pos2
     let pos2 = document.getElementById('pos2');
     pos2.addEventListener('click', () => {
          fadeInOut();
          displayNone();
          displayON();
          setTimeout(() => {
               camera.position.set(0, 75, 0.5);
               camera.rotation.set(0, 0, 0);
               controls.target.set(0, 75, 0);
               controls.enabled = true;
          }, 1000)
     });

     //Pos3
     let pos3 = document.getElementById('pos3');
     pos3.addEventListener('click', () => {
          fadeInOut();
          displayNone();
          displayON();
          setTimeout(() => {
               camera.position.set(-100, 40, -250.5);
               camera.rotation.set(0, 10, 0);
               controls.target.set(-100, 40, -250);
               controls.enabled = true;
          }, 1000)
     });

     //Pos4
     let pos4 = document.getElementById('pos4');
     pos4.addEventListener('click', () => {
          fadeInOut();
          displayNone();
          displayON();
          setTimeout(() => {
               //camera.position.set(0, 150, 0);
               //camera.rotation.set(1, 4.75, 0);
               camera.position.set(0, 300, -175);
               camera.rotation.set(300, 0, 300.02);
               controls.enabled = true;
          }, 1000)
     });

     //Sam
     let pos5 = document.getElementById('pos5');
     pos5.addEventListener('click', () => {
          fadeInOut();
          displayNone();
          displayON();
          setTimeout(() => {
               camera.position.set(60, 10, -22.5);
               camera.rotation.set(0, 4.75, 0);
               controls.enabled = false;
          }, 1000)
     });
}

function fadeInOut() {
     let fadeInOut = document.getElementById('fadeInOut');
     fadeInOut.style.zIndex = 9;
     fadeInOut.style.opacity = 1;
     setTimeout(() => {
          fadeInOut.style.zIndex = 0;
          fadeInOut.style.opacity = 0;
     }, 1000)
}
