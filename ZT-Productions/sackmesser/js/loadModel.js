

function sackmesserEnvironment() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/sackmesser_environment/SM_landscape.glb', function( gltf ) {
          envModel = gltf.scene;
          envModel.scale.set(100, 100, 100);
          envModel.rotation.set(0, -4.75, 0);
          envModel.position.set(0, 0, 0);
          scene.add( envModel );
          //Add Shadow
          gltf.scene.traverse( function ( child ) {

              if ( child.isMesh ) {

                  child.castShadow = true;
                  child.receiveShadow = true;

                }
              })
         }, manager.onProgress, manager.onError );
}//Ende sackmesserEnvironment

function sackmesserEnvironment2() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/sackmesser_environment/AM_Landscape_animiert.glb', function( gltf ) {
          envModel2 = gltf.scene;
          envModel2.scale.set(100, 100, 100);
          envModel2.rotation.set(0, -4.75, 0);
          envModel2.position.set(0, 0, 0);
          scene.add( envModel2 );
          const animation = gltf.animations[ 0 ];
          const mixer = new THREE.AnimationMixer( envModel2 );
          mixers.push( mixer );
          const action = mixer.clipAction( animation );
          action.play();
          //Add Shadow
          gltf.scene.traverse( function ( child ) {

              if ( child.isMesh ) {

                  child.castShadow = true;
                  child.receiveShadow = true;

                }
              })
         }, manager.onProgress, manager.onError );
}//Ende sackmesserEnvironment2


function zoomPoint1() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/symbol/AM_Sackmessersymbol_Alltag.glb', function( gltf ) {
          zoomPoint1 = gltf.scene;
          zoomPoint1.scale.set(100, 100, 100);
          zoomPoint1.position.set(-5, 15, 85);
          scene.add( zoomPoint1 );
          const animation = gltf.animations[ 0 ];
          const mixer = new THREE.AnimationMixer( zoomPoint1 );
          mixers.push( mixer );
          const action = mixer.clipAction( animation );
          action.play();
          let domEvents = new THREEx.DomEvents(camera, renderer.domElement);
          domEvents.addEventListener(zoomPoint1, "click", event => {
               zoomLib1();
               modelAlltagsmesser();
               textBoxAlltagsmesser.style.opacity = 1;
               textBoxAlltagsmesser.style.zIndex = 1;
          }); //Ende DomEvent
          //Add Shadow
          gltf.scene.traverse( function ( child ) {

              if ( child.isMesh ) {

                  child.castShadow = true;
                  child.receiveShadow = true;

                }
              })
         }, manager.onProgress, manager.onError );
}//Ende zoomPoint1

function zoomPoint2() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/symbol/AM_Sackmessersymbol_Foerster.glb', function( gltf ) {
          zoomPoint2 = gltf.scene;
          zoomPoint2.scale.set(100, 100, 100);
          zoomPoint2.position.set(-90, 20, 10);
          scene.add( zoomPoint2 );
          const animation = gltf.animations[ 0 ];
          const mixer = new THREE.AnimationMixer( zoomPoint2 );
          mixers.push( mixer );
          const action = mixer.clipAction( animation );
          action.play();
          let domEvents = new THREEx.DomEvents(camera, renderer.domElement);
          domEvents.addEventListener(zoomPoint2, "click", event => {
               zoomLib2();
               modelFoerstermesser();
               textBoxFoerstermesser.style.opacity = 1;
               textBoxFoerstermesser.style.zIndex = 1;
          }); //Ende DomEvent
          //Add Shadow
          gltf.scene.traverse( function ( child ) {

              if ( child.isMesh ) {

                  child.castShadow = true;
                  child.receiveShadow = true;

                }
              })
         }, manager.onProgress, manager.onError );
}//Ende zoomPoint2


function zoomPoint3() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/symbol/AM_Sackmessersymbol_Kletterer.glb', function( gltf ) {
          zoomPoint3 = gltf.scene;
          zoomPoint3.scale.set(100, 100, 100);
          zoomPoint3.position.set(0, 40, -85);
          scene.add( zoomPoint3 );
          const animation = gltf.animations[ 0 ];
          const mixer = new THREE.AnimationMixer( zoomPoint3 );
          mixers.push( mixer );
          const action = mixer.clipAction( animation );
          action.play();
          let domEvents = new THREEx.DomEvents(camera, renderer.domElement);
          domEvents.addEventListener(zoomPoint3, "click", event => {
               zoomLib3();
               modelKlettermesser();
               textBoxKlettermesser.style.opacity = 1;
               textBoxKlettermesser.style.zIndex = 1;
          }); //Ende DomEvent
          //Add Shadow
          gltf.scene.traverse( function ( child ) {

              if ( child.isMesh ) {

                  child.castShadow = true;
                  child.receiveShadow = true;

                }
              })
         }, manager.onProgress, manager.onError );
}//Ende zoomPoint3

function zoomPoint4() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/symbol/AM_Sackmessersymbol_Elektriker.glb', function( gltf ) {
          zoomPoint4 = gltf.scene;
          zoomPoint4.scale.set(100, 100, 100);
          zoomPoint4.position.set(90, 15, 0);
          scene.add( zoomPoint4 );
          const animation = gltf.animations[ 0 ];
          const mixer = new THREE.AnimationMixer( zoomPoint4 );
          mixers.push( mixer );
          const action = mixer.clipAction( animation );
          action.play();
          let domEvents = new THREEx.DomEvents(camera, renderer.domElement);
          domEvents.addEventListener(zoomPoint4, "click", event => {
               zoomLib4();
               modelElektrikermesser();
               textBoxElektrikermesser.style.opacity = 1;
               textBoxElektrikermesser.style.zIndex = 1;
          }); //Ende DomEvent
          //Add Shadow
          gltf.scene.traverse( function ( child ) {

              if ( child.isMesh ) {

                  child.castShadow = true;
                  child.receiveShadow = true;

                }
              })
         }, manager.onProgress, manager.onError );
}//Ende zoomPoint4


function modelAlltagsmesser() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/Animation_Maya_SM_A_finalX_3.glb', function( gltf ) {
          model1 = gltf.scene;
          model1.scale.set(8, 8, 8);
          model1.position.set(-5, 7.5, 80);
          model1.rotation.set(1.25, 0, 0);
          scene.add( model1 );
          const mixer = new THREE.AnimationMixer( model1 );
          mixers.push( mixer );

          const playAvAnimation0 = gltf.animations[ 0 ];
          const playAvAnimation1 = gltf.animations[ 1 ];
          const playAvAnimation2 = gltf.animations[ 2 ];
          const playAvAnimation3 = gltf.animations[ 3 ];
          const playAvAnimation4 = gltf.animations[ 4 ];
          const playAvAnimation5 = gltf.animations[ 5 ];
          const playAvAnimation6 = gltf.animations[ 6 ];
          const playAvAnimation7 = gltf.animations[ 7 ];

          avAnimation0.addEventListener( 'click', play0, false);
          avAnimation1.addEventListener( 'click', play1, false);
          avAnimation2.addEventListener( 'click', play2, false);
          avAnimation3.addEventListener( 'click', play3, false);
          avAnimation4.addEventListener( 'click', play4, false);
          avAnimation5.addEventListener( 'click', play5, false);
          avAnimation6.addEventListener( 'click', play6, false);

          function play0( event ) {
               const action = mixer.clipAction( playAvAnimation0 );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }

          function play1( event ) {
               const action = mixer.clipAction( playAvAnimation1 );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }

          function play2( event ) {
               const action = mixer.clipAction( playAvAnimation2 );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }

          function play3( event ) {
               const action = mixer.clipAction( playAvAnimation3 );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }

          function play4( event ) {
               const action = mixer.clipAction( playAvAnimation4 );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }

          function play5( event ) {
               const action = mixer.clipAction( playAvAnimation5 );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }

          function play6( event ) {
               const action1 = mixer.clipAction( playAvAnimation6 );
               action1.setLoop( THREE.LoopOnce );
               action1.play();
               action1.reset();
               const action2 = mixer.clipAction( playAvAnimation7 );
               action2.setLoop( THREE.LoopOnce );
               action2.play();
               action2.reset();
          }

     //Add Shadow
/*     gltf.scene.traverse( function ( child ) {

          if ( child.isMesh ) {

               child.castShadow = true;
               child.receiveShadow = true;

          }
     })*/
     }, manager.onProgress, manager.onError );
}

function modelFoerstermesser() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/Animation_Maya_SM_F_finalX_3.glb', function( gltf ) {
          model2 = gltf.scene;
          model2.position.set(-92.5, 12.5, 10);
          model2.scale.set( 8, 8, 8);
          model2.rotation.set(1.25, 0, 0);
          model2.castShadow = true;
          model2.receiveShadow = true;
          scene.add( model2 );
          const mixer = new THREE.AnimationMixer( model2 );
          mixers.push( mixer );

          const playFvAnimation0 = gltf.animations[ 0 ];
          const playFvAnimation1 = gltf.animations[ 1 ];
          const playFvAnimation2 = gltf.animations[ 2 ];
          const playFvAnimation3 = gltf.animations[ 3 ];
          const playFvAnimation4 = gltf.animations[ 4 ];
          const playFvAnimation5 = gltf.animations[ 5 ];
          const playFvAnimation6 = gltf.animations[ 6 ];


          fvAnimation0.addEventListener( 'click', play0, false);
          fvAnimation1.addEventListener( 'click', play1, false);
          fvAnimation2.addEventListener( 'click', play2, false);
          fvAnimation3.addEventListener( 'click', play3, false);
          fvAnimation4.addEventListener( 'click', play4, false);
          fvAnimation5.addEventListener( 'click', play5, false);
          fvAnimation6.addEventListener( 'click', play6, false);


          function play0( event ) {
               const action = mixer.clipAction( playFvAnimation0 );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }

          function play1( event ) {
               const action = mixer.clipAction( playFvAnimation1 );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }

          function play2( event ) {
               const action = mixer.clipAction( playFvAnimation2 );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }

          function play3( event ) {
               const action = mixer.clipAction( playFvAnimation3 );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }

          function play4( event ) {
               const action = mixer.clipAction( playFvAnimation4 );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }

          function play5( event ) {
               const action = mixer.clipAction( playFvAnimation5 );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }

          function play6( event ) {
               const action = mixer.clipAction( playFvAnimation6 );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }

          //Add Shadow
/*          gltf.scene.traverse( function ( child ) {

              if ( child.isMesh ) {

                  child.castShadow = true;
                  child.receiveShadow = true;

                }
           })*/
         }, manager.onProgress, manager.onError );
}

function modelKlettermesser() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/Animation_Maya_SM_K_finalX_3.glb', function( gltf ) {
          model3 = gltf.scene;
          model3.scale.set(8, 8, 8);
          model3.position.set(-5, 37.5, -85);
          model3.rotation.set(1.25, 0, 0);
          scene.add( model3 );

          const mixer = new THREE.AnimationMixer( model3 );
          mixers.push( mixer );

          const playKvAnimation0 = gltf.animations[ 0 ];
          const playKvAnimation1 = gltf.animations[ 1 ];
          const playKvAnimation2 = gltf.animations[ 2 ];
          const playKvAnimation3 = gltf.animations[ 3 ];
          const playKvAnimation4 = gltf.animations[ 4 ];
          const playKvAnimation5 = gltf.animations[ 5 ];


          kvAnimation0.addEventListener( 'click', play0, false);
          kvAnimation2.addEventListener( 'click', play2, false);
          kvAnimation4.addEventListener( 'click', play4, false);
          kvAnimation5.addEventListener( 'click', play5, false);


          function play0( event ) {
               const action1 = mixer.clipAction( playKvAnimation0 );
               action1.setLoop( THREE.LoopOnce );
               action1.play();
               action1.reset();
               const action2 = mixer.clipAction( playKvAnimation1 );
               action2.setLoop( THREE.LoopOnce );
               action2.play();
               action2.reset();
          }

          function play2( event ) {
               const action1 = mixer.clipAction( playKvAnimation2 );
               action1.setLoop( THREE.LoopOnce );
               action1.play();
               action1.reset();
               const action2 = mixer.clipAction( playKvAnimation3 );
               action2.setLoop( THREE.LoopOnce );
               action2.play();
               action2.reset();
          }

          function play4( event ) {
               const action = mixer.clipAction( playKvAnimation4 );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }

          function play5( event ) {
               const action = mixer.clipAction( playKvAnimation5 );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }

          //Add Shadow
/*          gltf.scene.traverse( function ( child ) {

              if ( child.isMesh ) {

                  child.castShadow = true;
                  child.receiveShadow = true;

                }
           })*/
         }, manager.onProgress, manager.onError );
}

function modelElektrikermesser() {
     const loader = new THREE.GLTFLoader(manager);
     loader.load( 'models/Animation_Maya_SM_E_finalX_3.glb', function( gltf ) {
          model4 = gltf.scene;
          model4.scale.set(8, 8, 8);
          model4.position.set(90, 7.5, 0);
          model4.rotation.set(1.25, 0, 0);
          scene.add( model4 );

          const mixer = new THREE.AnimationMixer( model4 );
          mixers.push( mixer );

          const playEvAnimation0 = gltf.animations[ 0 ];
          const playEvAnimation1 = gltf.animations[ 1 ];
          const playEvAnimation2 = gltf.animations[ 2 ];
          const playEvAnimation3 = gltf.animations[ 3 ];
          const playEvAnimation4 = gltf.animations[ 4 ];
          const playEvAnimation5 = gltf.animations[ 5 ];
          const playEvAnimation6 = gltf.animations[ 6 ];
          const playEvAnimation7 = gltf.animations[ 7 ];

          evAnimation0.addEventListener( 'click', play0, false);
          evAnimation3.addEventListener( 'click', play3, false);
          evAnimation4.addEventListener( 'click', play4, false);
          evAnimation5.addEventListener( 'click', play5, false);
          evAnimation6.addEventListener( 'click', play6, false);


          function play0( event ) {
               const action1 = mixer.clipAction( playEvAnimation0 );
               action1.setLoop( THREE.LoopOnce );
               action1.play();
               action1.reset();
               const action2 = mixer.clipAction( playEvAnimation1 );
               action2.setLoop( THREE.LoopOnce );
               action2.play();
               action2.reset();
               const action3 = mixer.clipAction( playEvAnimation2 );
               action3.setLoop( THREE.LoopOnce );
               action3.play();
               action3.reset();
          }

          function play3( event ) {
               const action = mixer.clipAction( playEvAnimation3 );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }

          function play4( event ) {
               const action = mixer.clipAction( playEvAnimation4 );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }

          function play5( event ) {
               const action = mixer.clipAction( playEvAnimation5 );
               action.setLoop( THREE.LoopOnce );
               action.play();
               action.reset();
          }

          function play6( event ) {
               const action1 = mixer.clipAction( playEvAnimation6 );
               action1.setLoop( THREE.LoopOnce );
               action1.play();
               action1.reset();
               const action2 = mixer.clipAction( playEvAnimation7 );
               action2.setLoop( THREE.LoopOnce );
               action2.play();
               action2.reset();
          }

          //Add Shadow
/*          gltf.scene.traverse( function ( child ) {

              if ( child.isMesh ) {

                  child.castShadow = true;
                  child.receiveShadow = true;

                }
           })*/
         }, manager.onProgress, manager.onError );
}
