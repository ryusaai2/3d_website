function navShowModel() {

     let alltagsmesser = document.getElementById('alltagsmesser');
     alltagsmesser.addEventListener('click', () => {
          modelAlltagsmesser();
          zoomLib1();
          textBoxAlltagsmesser.style.opacity = 1;
          textBoxAlltagsmesser.style.zIndex = 1;
     });

     let foerstermesser = document.getElementById('foerstermesser');
     foerstermesser.addEventListener('click', () => {
          modelFoerstermesser();
          zoomLib2();
          textBoxFoerstermesser.style.opacity = 1;
          textBoxFoerstermesser.style.zIndex = 1;
     });

     let klettermesser = document.getElementById('klettermesser');
     klettermesser.addEventListener('click', () => {
          modelKlettermesser();
          zoomLib3();
          textBoxKlettermesser.style.opacity = 1;
          textBoxKlettermesser.style.zIndex = 1;
     });

     let elektrikermesser = document.getElementById('elektrikermesser');
     elektrikermesser.addEventListener('click', () => {
          modelElektrikermesser();
          zoomLib4();
          textBoxElektrikermesser.style.opacity = 1;
          textBoxElektrikermesser.style.zIndex = 1;
     });

}
