//FPS anzeige
//(function(){var script=document.createElement('script');script.onload=function(){var stats=new Stats();document.body.appendChild(stats.dom);requestAnimationFrame(function loop(){stats.update();requestAnimationFrame(loop)});};script.src='//mrdoob.github.io/stats.js/build/stats.min.js';document.head.appendChild(script);})()

//Alle Variablen
let container;
let camera;
let controls;
let renderer;
let scene;
let modelLoad;

const clock = new THREE.Clock();
const manager = initLoadingManager(); //LadeScreen
const mixers = []; //Animation

function init() {
     container = document.querySelector( '#scene-container' );
     scene = new THREE.Scene();

     createCamera();
     createControls();
     createLights();
     createRenderer();
     loadModels();

     renderer.setAnimationLoop( () => {
          update();
          render();
     } );
}

//Kamera
function createCamera() {
     camera = new THREE.PerspectiveCamera( 45, container.clientWidth / container.clientHeight, 1, 5000 );
     camera.position.set(0, 75, 75);
     camera.rotation.set(0, 0, 0);
}

//Kontrolle
function createControls() {
     controls = new THREE.OrbitControls( camera, container );
     controls.enableZoom = false;
     controls.enablePan = true;
     controls.enableRotate = false;
     controls.target.set(0, 0, 0);
     controls.mouseButtons = {
          LEFT: THREE.MOUSE.RIGHT,
          MIDDLE: THREE.MOUSE.MIDDLE,
          RIGHT: THREE.MOUSE.LEFT
     }
     controls.update();
}

//Licht
function createLights() {
     dirLight = new THREE.DirectionalLight( 0xffffff, 2 );
     dirLight.color.setHSL( 0.1, 1, 0.95 );
     dirLight.position.set( 0, 10, 5 );
     dirLight.position.multiplyScalar( 30 );
     dirLight.castShadow = true;
     scene.add( dirLight );

     spotLight = new THREE.SpotLight( 0xffffff, 2 );
     spotLight.rotation.set(0, 0, 90);
     spotLight.position.set( 110, 30, 5 );
     //scene.add( spotLight );

     scene.fog = new THREE.FogExp2( 0xfffce7, 0.005);
     scene.background = new THREE.Color(0xffffff, 2);

     const light = new THREE.PointLight(0xFFFFFF, 1);
     light.castShadow = true;
     light.position.set(0, 100, 0);
     light.shadow.mapSize.width = 2048;
     light.shadow.mapSize.height = 2048;
     scene.add(light);

     //Skybox
     var geometry = new THREE.CubeGeometry( 1000, 1000, 1000 );
     var cubeMaterials = [
          new THREE.MeshBasicMaterial( { map: new THREE.TextureLoader( ).load( "skybox/front.png" ), side: THREE.DoubleSide } ),
          new THREE.MeshBasicMaterial( { map: new THREE.TextureLoader( ).load( "skybox/front.png" ), side: THREE.DoubleSide } ),
          new THREE.MeshBasicMaterial( { map: new THREE.TextureLoader( ).load( "skybox/front.png" ), side: THREE.DoubleSide } ),
          new THREE.MeshBasicMaterial( { map: new THREE.TextureLoader( ).load( "skybox/front.png" ), side: THREE.DoubleSide } ),
          new THREE.MeshBasicMaterial( { map: new THREE.TextureLoader( ).load( "skybox/front.png" ), side: THREE.DoubleSide } ),
          new THREE.MeshBasicMaterial( { map: new THREE.TextureLoader( ).load( "skybox/front.png" ), side: THREE.DoubleSide } )
     ];

     var cubeMaterial = new THREE.MeshFaceMaterial( cubeMaterials );
     var cube = new THREE.Mesh( geometry, cubeMaterial );
     cube.position.set(0, 0, 0);
     scene.add( cube );



}

//Model laden
function loadModels() {
     sackmesserEnvironment();
     sackmesserEnvironment2();
     zoomPoint1();
     zoomPoint2();
     zoomPoint3();
     zoomPoint4();
     navShowModel();
}

//Rendern
function createRenderer() {
     renderer = new THREE.WebGLRenderer( { antialias: true } );
     renderer.setSize( container.clientWidth, container.clientHeight );
     renderer.setPixelRatio( window.devicePixelRatio );
     renderer.shadowMap.enabled = true;
     renderer.shadowMap.type = THREE.PCSoftShadowMap;
     container.appendChild( renderer.domElement );
}

function update() {
     //Animation abspielen
     const delta = clock.getDelta();
     for ( const mixer of mixers ) {
          mixer.update( delta );
     }

     //Zoomen
     TWEEN.update();
     requestAnimationFrame( update );
}

function render() {
     renderer.render( scene, camera );
}

//Responsive
function onWindowResize() {
     camera.aspect = container.clientWidth / container.clientHeight;
     camera.updateProjectionMatrix();
     renderer.setSize( container.clientWidth, container.clientHeight );
}

window.addEventListener( 'resize', onWindowResize )

init();


///////////////////////////////////////////////////////////////////////////////////

//LadeScreen
function initLoadingManager() {
     const manager = new THREE.LoadingManager();
     const progressBar = document.querySelector( '#progress' );
     const loadingOverlay = document.querySelector( '#loading-overlay' );
     let percentComplete = 1;
     let frameID = null;
     const updateAmount = 12.35; // in percent of bar width, should divide 100 evenly
     const animateBar = () => {
          percentComplete += updateAmount;
          progressBar.style.width = percentComplete + '%';
          frameID = requestAnimationFrame( animateBar )
     }
     manager.onStart = () => { // prevent the timer being set again, if onStart is called multiple times
          if ( frameID !== null ) return;
              animateBar();
     };
     manager.onLoad = () => { // reset the bar in case we need to use it again
          loadingOverlay.classList.add( 'loading-overlay-hidden' );
          percentComplete = 0;
          progressBar.style.width = 0;
          cancelAnimationFrame( frameID );
     };
     manager.onError = function ( e ) {
          console.error( e );
          progressBar.style.backgroundColor = 'red';
     }
          return manager;
}

///////////////////////////////////////////////////////////////////////////////////
//Alle eigene Variablen

let textBoxAlltagsmesser = document.getElementById('textbox-alltagsmesser');
let textBoxFoerstermesser = document.getElementById('textbox-foerstermesser');
let textBoxKlettermesser = document.getElementById('textbox-klettermesser');
let textBoxElektrikermesser = document.getElementById('textbox-elektrikermesser');

let button = document.getElementById('zoom-out');
let arrowLeft = document.getElementById('arrow-left');
let arrowRight = document.getElementById('arrow-right');
let showNewPosition = document.getElementById('show-new-position');
let playAnimation = document.getElementById('playAnimation');
let stopAnimation = document.getElementById('stopAnimation');
let showLiveDemo = document.getElementById('show-live-demo');
let liveDemoPage = document.getElementById('live-demo-page');
let backTo3D = document.getElementById('backTo3D');
let sceneContainer = document.getElementById('scene-container');
let customPosition = document.getElementById('custom-position');
let customPositionActive = document.getElementById('custom-position-active');
let customView = document.getElementById('custom-view');
let customViewActive = document.getElementById('custom-view-active');
let navToggle = document.getElementById('nav-toggle');

//Alltagsmesser
let avAnimation0 = document.getElementById('avAnimation0');
let avAnimation1 = document.getElementById('avAnimation1');
let avAnimation2 = document.getElementById('avAnimation2');
let avAnimation3 = document.getElementById('avAnimation3');
let avAnimation4 = document.getElementById('avAnimation4');
let avAnimation5 = document.getElementById('avAnimation5');
let avAnimation6 = document.getElementById('avAnimation6');
let avAnimation7 = document.getElementById('avAnimation7');

//Förstermesser
let fvAnimation0 = document.getElementById('fvAnimation0');
let fvAnimation3 = document.getElementById('fvAnimation3');
let fvAnimation4 = document.getElementById('fvAnimation4');
let fvAnimation5 = document.getElementById('fvAnimation5');
let fvAnimation6 = document.getElementById('fvAnimation6');

//Klettermesser
let kvAnimation0 = document.getElementById('kvAnimation0');
let kvAnimation1 = document.getElementById('kvAnimation1');
let kvAnimation2 = document.getElementById('kvAnimation2');
let kvAnimation3 = document.getElementById('kvAnimation3');

//Elektrikermesser
let evAnimation0 = document.getElementById('evAnimation0');
let evAnimation3 = document.getElementById('evAnimation3');
let evAnimation4 = document.getElementById('evAnimation4');
let evAnimation5 = document.getElementById('evAnimation5');
let evAnimation6 = document.getElementById('evAnimation6');

//Accordion
jQuery('.custom_faq__accordian-heading').click(function(e){
       e.preventDefault();
       if (!jQuery(this).hasClass('active')) {
            jQuery('.custom_faq__accordian-heading').removeClass('active');
            jQuery('.custom_faq__accordion-content').slideUp(500);
            jQuery(this).addClass('active');
            jQuery(this).next('.custom_faq__accordion-content').slideDown(500);
       }
       else if (jQuery(this).hasClass('active')) {
            jQuery(this).removeClass('active');
            jQuery(this).next('.custom_faq__accordion-content').slideUp(500);
       }
    });
