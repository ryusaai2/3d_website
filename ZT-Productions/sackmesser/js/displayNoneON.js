function displayNone() {
     //arrowLeft.style.display = 'none';
     //arrowRight.style.display = 'none';
     button.style.display = 'none';
     //navToggle.style.display = 'none';

     textBoxAlltagsmesser.style.opacity = 0;
     textBoxAlltagsmesser.style.zIndex = 0;
     textBoxFoerstermesser.style.opacity  = 0;
     textBoxFoerstermesser.style.zIndex = 0;
     textBoxKlettermesser.style.opacity  = 0;
     textBoxKlettermesser.style.zIndex = 0;
     textBoxElektrikermesser.style.opacity  = 0;
     textBoxElektrikermesser.style.zIndex = 0;

     scene.remove( zoomPoint1 );
     scene.remove( zoomPoint2 );
     scene.remove( zoomPoint3 );
     scene.remove( zoomPoint4 );
}

function displayON() {
     //navToggle.style.display = 'block';

     scene.add( zoomPoint1 );
     scene.add( zoomPoint2 );
     scene.add( zoomPoint3 );
     scene.add( zoomPoint4 );
}
