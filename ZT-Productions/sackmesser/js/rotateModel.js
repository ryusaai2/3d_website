function rotateModel(model) {
     function toRadians(angle) {
     	return angle * (Math.PI / 180);
     }

     function toDegrees(angle) {
     	return angle * (180 / Math.PI);
     }

     let isDragging = false;
     let previousMousePosition = {
         x: 0,
         y: 0
     };
     $(renderer.domElement).on('mousedown', function(e) {
         isDragging = true;
     })
     .on('mousemove', function(e) {
         let deltaMove = {
             x: e.offsetX-previousMousePosition.x,
             y: e.offsetY-previousMousePosition.y
         };

         if(isDragging) {

             let deltaRotationQuaternion = new THREE.Quaternion()
                 .setFromEuler(new THREE.Euler(
                     toRadians(deltaMove.y * 0.25),
                     toRadians(deltaMove.x * 0.25),
                     0,
                     'XYZ'
                 ));
                 if(model == 'rotateModel1') {
                      model1.quaternion.multiplyQuaternions(deltaRotationQuaternion, model1.quaternion);
                 }
                 else if (model == 'rotateModel2') {
                      model2.quaternion.multiplyQuaternions(deltaRotationQuaternion, model2.quaternion);
                 }
                 else if (model == 'rotateModel3') {
                      model3.quaternion.multiplyQuaternions(deltaRotationQuaternion, model3.quaternion);
                 }
                 else if (model == 'rotateModel4') {
                      model4.quaternion.multiplyQuaternions(deltaRotationQuaternion, model4.quaternion);
                 }


         }

         previousMousePosition = {
             x: e.offsetX,
             y: e.offsetY
         };
     });


     //Maus loslassen
     $(document).on('mouseup', function(e) {
         isDragging = false;
     });


     // shim layer with setTimeout fallback
     window.requestAnimationFrame = (function(){
         return  window.requestAnimationFrame ||
             window.webkitRequestAnimationFrame ||
             window.mozRequestAnimationFrame ||
             function(callback) {
                 window.setTimeout(callback, 1000 / 60);
             };
     })();

}
