//zoomLib1
function zoomLib1() {
     camera.position.set(2.5, 52.5, 130);
     camera.rotation.set(0, 0, 0);
     controls.target.set(2.5, 7.5, 85);
     controls.enablePan = false;
     displayNone();
     //addArrow1();
     zoomIn(0, -45, -30);
     rotateModel('rotateModel1');
     //button um zurückzukehren
     let zoomOut1 = document.getElementById('zoom-out');
     zoomOut1.style.display = 'block';
     //button click event
     zoomOut1.addEventListener('click', () => {
          displayNone();
          displayON();
          camera.position.set(-5, 0, 100);
          camera.rotation.set(125, 0, 0);
          zoomOut(0, 60, 50);
          scene.remove( model1 );
          setTimeout(function() {
               controls.enablePan = true;
          }, 1500);

     })//Ende Close Button
} // Ende zoomLib1

//zoomLib2
function zoomLib2() {
     camera.position.set(-87.5, 57.5, 50);
     camera.rotation.set(0, 0, 0);
     controls.target.set(-87.5, 12.5, 0);
     controls.enablePan = false;
     displayNone();
     //addArrow2();
     zoomIn(0, -45, -25);
     rotateModel('rotateModel2');
     //button um zurückzukehren
     let zoomOut2 = document.getElementById('zoom-out');
     zoomOut2.style.display = 'block';
     //button click event
     zoomOut2.addEventListener('click', () => {
          displayNone();
          displayON();
          camera.position.set(-87.5, 12.5, 25);
          camera.rotation.set(125, 0, 0);
          zoomOut(0, 60, 50);
          scene.remove( model2 );
          setTimeout(function() {
               controls.enablePan = true;
          }, 1500);
     })//Ende Close Button
} // Ende zoomLib2

//zoomLib3
function zoomLib3() {
     camera.position.set(0, 82.5, -42.5);
     camera.rotation.set(0, 0, 0);
     controls.target.set(0, 25, -42.5);
     controls.enablePan = false;
     displayNone();
     //addArrow3();
     zoomIn(0, -45, -25);
     rotateModel('rotateModel3');
     //button um zurückzukehren
     let zoomOut3 = document.getElementById('zoom-out');
     zoomOut3.style.display = 'block';
     //button click event
     zoomOut3.addEventListener('click', () => {
          displayNone();
          displayON();
          camera.position.set(0, 25, -42.5);
          camera.rotation.set(125, 0, 0);
          zoomOut(0, 60, 50);
          scene.remove( model3 );
          setTimeout(function() {
               controls.enablePan = true;
          }, 1500);
     })//Ende Close Button
} // Ende zoomLib3

//zoomLib4
function zoomLib4() {
     camera.position.set(97.5, 52.5, 45);
     camera.rotation.set(0, 0, 0);
     controls.target.set(97.5, 7.5, 0);
     controls.enablePan = false;
     displayNone();
     //addArrow4();
     zoomIn(0, -45, -25);
     rotateModel('rotateModel4');
     //button um zurückzukehren
     let zoomOut4 = document.getElementById('zoom-out');
     zoomOut4.style.display = 'block';
     //button click event
     zoomOut4.addEventListener('click', () => {
          displayNone();
          displayON();
          camera.position.set(97.5, 7.5, 25);
          camera.rotation.set(125, 0, 0);
          zoomOut(0, 60, 50);
          scene.remove( model4 );
          setTimeout(function() {
               controls.enablePan = true;
          }, 1500);
     })//Ende Close Button
} // Ende zoomLib4
