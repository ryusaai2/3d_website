function zoomIn(x, y, z) {
     //Zoom in animation
     let from = {
       x: camera.position.x,
       y: camera.position.y,
       z: camera.position.z
     };

     //position nach klick
     let to = {
          x: camera.position.x + x,
          y: camera.position.y + y,
          z: camera.position.z + z
     };

     let tween = new TWEEN.Tween(from)
          .to(to, 1000) //Zeit für animation
          .easing(TWEEN.Easing.Linear.None)
          .onUpdate(function () {
          camera.position.set(this.x, this.y, this.z);
          })
          .onComplete(function () {
          })
          .start();

}//End zoomIn

function zoomOut(x, y, z) {
     //Zoom out animation
     let from = {
          x: camera.position.x,
          y: camera.position.y,
          z: camera.position.z
     };

     //position nach klick
     let to = {
          x: camera.position.x + x,
          y: camera.position.y + y,
          z: camera.position.z + z
     };
     let tween = new TWEEN.Tween(from)
     .to(to, 1000) //Zeit für animation
     .easing(TWEEN.Easing.Linear.None)
     .onUpdate(function () {
     camera.position.set(this.x, this.y, this.z);
     })
     .onComplete(function () {})
     .start();
}//End zoomOut
